import { Injectable } from '@nestjs/common';
import form from './js/form.service'

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World, it\'s me!';
  }

  display() {
    const logicForm = new form();
    return logicForm.addBooks()
  }
}
