import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Books } from '../../Domain/Interfaces/books.interface';
import { CreateBooksDTO } from '../../Domain/Dto/create-books.dto';

@Injectable()
export class BooksService {
    /**
     * 
     * @param booksModel 
     * We inject the BooksModelto the BooksService , useful for updating retrieving,... books data
     */
    constructor(@InjectModel('Books') private readonly booksModel: Model<Books>) { }
    // fetch all Books
    async getAllBooks(): Promise<Books[]> {
        const books = await this.booksModel.find().exec();
        return books;
    }
    // Get a single Book
    async getBook(bookID): Promise<Books> {
        const books = await this.booksModel.findById(bookID).exec();
        return books;
    }
    // post a single Book
    async addBook(CreateBooksDTO: CreateBooksDTO): Promise<Books> {
        const newBook = await new this.booksModel(CreateBooksDTO);
        return newBook.save();
    }
    // Edit Books details
    async updateBook(bookID, CreateBooksDTO: CreateBooksDTO): Promise<Books> {
        const updateBook = await this.booksModel
            .findByIdAndUpdate(bookID, CreateBooksDTO, { new: true });
        return updateBook;
    }
    // Delete a Books
    async deleteBook(bookID): Promise<any> {
        const deletedBook= await this.booksModel.findByIdAndRemove(bookID);
        return deletedBook;
    }
}