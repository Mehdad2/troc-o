export class CreateBooksDTO {
    readonly isbn: string;
    readonly title_troc: string;
    readonly author: string;
    readonly publisher: string;
    readonly updated_at: Date;
    readonly description: string;
    readonly created_at: Date;
}