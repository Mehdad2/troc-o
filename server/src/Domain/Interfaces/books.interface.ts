import { Document } from 'mongoose';

// this Interface will check and determine the types of values received by the application
export interface Books extends Document {
    readonly isbn: string;
    readonly title_troc: string;
    readonly author: string;
    readonly publisher: string;
    readonly updated_at: Date;
    readonly description: string;
    readonly created_at: Date;
}