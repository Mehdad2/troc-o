import { Module } from '@nestjs/common';
import { BooksController } from '../../../Presentation/Controller/Books/books.controller';
import { BooksService } from '../../../Service/Books/books.service';
import { MongooseModule } from '@nestjs/mongoose';
import { BooksSchema } from '../../Schemas/books.schema';

/**
 * We define / set up ths books Model which will be registered in the BookModule
 * it will help to inject the book Model to the Book service with a specific decorator (see BooksService)
 */
@Module({
  imports: [
    MongooseModule.forFeature([{name:'Books', schema: BooksSchema }])
  ],
  controllers: [BooksController],
  providers: [BooksService],
  exports: [BooksService, MongooseModule],
  
})
export class BooksModule {}