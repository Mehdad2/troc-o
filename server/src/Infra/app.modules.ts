import { Module,  NestModule, RequestMethod, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from '../Presentation/Controller/app.controller';
import{ BooksController} from '../Presentation/Controller/Books/books.controller';
import { AppService } from '../Application/app.service';
import { BooksService } from '../Application/Books/books.service';
import { BooksModule } from '../Infra/Modules/Books/books.module';
import { MongooseModule } from '@nestjs/mongoose';


/**
 * Connection to MongoDb forRoot == mongoose.connect()
 */
@Module({
  imports: [ MongooseModule.forRoot('mongodb://localhost:27017/troc-app', { useNewUrlParser: true }),BooksModule],
  controllers: [AppController],
  providers: [AppService],

})

export class AppModule  {}


/**
 * For using a Middleware we need to implment 
 * NesModule interface
 * Here we set up FrontendMiddleware for  All path route
 * Including the principal route defined in appController
 * We apply FrontendMiddleware to the method Get
 */


