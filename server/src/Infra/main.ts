import { NestFactory} from '@nestjs/core';
import { AppModule } from './app.modules';
import { BooksModule } from '../Infra/Modules/Books/books.module';

var hbs = require('hbs');


async function connect() {

  const app = await NestFactory.create(AppModule);
  

  // app.useStaticAssets({
  //   root: join(__dirname, '..' ,'Presentation', 'public'),
  //   prefix: '/public/',

  // });

  // app.setViewEngine({
  //   engine: {
  //     handlebars: require('hbs'),
  //   },
  //   templates: join(__dirname ,'..','Presentation','views'),
  // });

  // hbs.registerPartials(join(__dirname ,'..','Presentation','views','layout'));

  /**
   * Cross-Origin Resource Sharing
   * to allow request from the client side we enable Cors so that the client and server can interract event in different port
   */
  app.enableCors()
  // io.on('connection', function(socket) {
  //   console.log('a user connected')
  // })

  await app.listen(7000);

}
connect();
