import * as mongoose from 'mongoose';

//The schema determine which data will be stored in database, the shape of the documents inside a collection
export const BooksSchema = new mongoose.Schema({
    isbn: String,
    title_troc: String,
    author: String,
    publisher: String,
    image: String,
    description: String,
    created_at: { type: Date, default: Date.now },
    updated_at: { type: Date, default: Date.now }
})