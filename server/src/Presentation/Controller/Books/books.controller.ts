import { Controller, Get, Res, HttpStatus, Post, Body, Put, Query, NotFoundException, Delete, Param } from '@nestjs/common';
import { BooksService } from '../../../Service/Books/books.service'
import { CreateBooksDTO } from '../../../Domain/Dto/create-books.dto';

@Controller('books')
export class BooksController {
  //to interact with the database BooksServce is injected into constructor
    constructor(private bookService: BooksService) { }

    // add a book
    @Post('/create')
    async addBook(@Res() res, @Body() CreateBooksDTO: CreateBooksDTO) {
        const book = await this.bookService.addBook(CreateBooksDTO)
        return res.status(HttpStatus.OK).json({
            message: "Books has been created successfully",
            book
        })
    }

    // Retrieve books list
    @Get('books')
    async getAllCustomer(@Res() res) {
        const books = await this.bookService.getAllBooks();
        return res.status(HttpStatus.OK).json(books);
    }

    // Fetch a particular book using ID
    @Get('book/:bookID')
    async getCustomer(@Res() res, @Param('bookID') bookID) {
        const book = await this.bookService.getBook(bookID);
        if (!book) throw new NotFoundException('Book does not exist!');
        return res.status(HttpStatus.OK).json(book);
    }

    //update a book's details

    @Put('/update')
    async updateBook(@Res() res, @Query('bookID') bookID, @Body() CreateBooksDto: CreateBooksDTO) {
      const book = await this.bookService.updateBook(bookID,CreateBooksDto);
      if (!book) throw new NotFoundException('Book does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Book has been successfully updated',
            book
        });
    }

    //Delete a book

    @Delete('/delete')
    async deleteBook(@Res() res, @Query('bookID') bookID) {
      const book = await this.bookService.deleteBook(bookID);
      if (!book) throw new NotFoundException('Book does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Book has been successfully updated',
            book
        });
    }
}