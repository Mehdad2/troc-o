import { Controller, Get, Render } from '@nestjs/common';
import { AppService } from '../../Application/app.service';


@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  /**
   * @Get Create an endpoint for route /
   */
  @Get()
  root() {
   return { message : this.appService.getHello(), test: 'tests' };
  }
}
