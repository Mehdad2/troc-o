import Vue from 'vue'
import Router from 'vue-router'
import HomeComponent from '@/views/Home';
import ManualComponent from '../components/form/Manual.vue';
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
   { path: '/', redirect: { name: 'home' } },
    { path: '/home', name: 'home', component: HomeComponent },
    { path: '/create', name: 'Manual', component: ManualComponent },
    //{ path: '/edit/:id', name: 'Edit', component: EditComponent },
  ]
});