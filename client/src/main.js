import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import router from './utils/router'

//We will use the vue Router component
Vue.use(VueRouter)

// Vue.config.productionTip = false
// new Vue({
//   render: h => h(App),
// }).$mount('#app')
//the data and the Dom are linked


const app = new Vue({
  el: '#app',
  router,
  render: h => h(App)
})

